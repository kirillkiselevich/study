package kpp.lab3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class N10 {
    public static void main(String[] args) throws FileNotFoundException {
        Set<String> words = new HashSet<>();
        File text = new File("C:\\Users\\qurub\\IdeaProjects\\JavaBook\\text.txt");
        long totalTime = 0;
        try (Scanner scanner= new Scanner(text)){
            while (scanner.hasNext()) {
                String word = scanner.next();
                long callTime = System.currentTimeMillis();
                words.add(word);
                callTime = System.currentTimeMillis() - callTime;
                totalTime += callTime;
            }
        }
        Iterator<String> iter = words.iterator();
        for (int i = 1;i <=20 && iter.hasNext(); i++)
            System.out.println(iter.next());
        System.out.println(".   .   .");
        System.out.println(words.size() + " distinct words. " + totalTime + " milliseconds.");
    }
}
