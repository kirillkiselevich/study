package kpp.lab3;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class N6 {
    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        Stack<Integer> stack1 = new Stack<>();
        stack1.push(0);
        stack1.push(1);
        stack1.push(2);
        Stack<Integer> stack2 = new Stack<>();
        stack2.push(3);
        stack2.push(4);
        stack2.push(5);
        System.out.println("stack #1" + stack1);
        System.out.println("stack #2" + stack2);
        while (!stack1.empty())
            list1.add(stack1.pop());
        while (!stack2.empty())
            list2.add(stack2.pop());
        stack1.clear();
        stack2.clear();
        for (int i = list2.size() - 1; i >= 0; i--) {
            stack1.push(list2.get(i));
        }
        for (int i = list1.size() - 1; i >= 0; i--) {
            stack2.push(list1.get(i));
        }
        System.out.println();
        System.out.println("stack #1" + stack1);
        System.out.println("stack #2" + stack2);


    }
}
