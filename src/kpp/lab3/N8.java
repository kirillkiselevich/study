package kpp.lab3;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class N8 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        try (FileReader fileReader = new FileReader(new File("C:\\Users\\qurub\\IdeaProjects\\Study\\text.txt"))) {
            BufferedReader reader = new BufferedReader(fileReader);
            String line = null;
            while ((line = reader.readLine()) != null) {
                list.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(list);
        System.out.println();
        Collections.sort(list);
        System.out.println(list);
    }
}
