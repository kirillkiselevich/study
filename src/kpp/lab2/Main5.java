package kpp.lab2;


import java.io.*;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class Main5 {
    static ArrayList<File> list = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        String path = "C:\\Users\\qurub\\Documents\\corejava";
        processFilesFromFolder(new File(path));
        ZipOutputStream zout = new ZipOutputStream(new FileOutputStream("C:\\Users\\qurub\\Documents\\java\\output.zip"));


        for (File file : list) {
            System.out.println(file.getName());
            System.out.println(file.getPath());
            FileInputStream fis = new FileInputStream(file.getPath());
            try {
                ZipEntry entry1 = new ZipEntry(file.getName());
                zout.putNextEntry(entry1);


            } catch (IOException e) {
                e.printStackTrace();
            }
            // считываем содержимое файла в массив byte
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            // добавляем содержимое к архиву
            zout.write(buffer);
            zout.closeEntry();
        }
        zout.close();

        // закрываем текущую запись для новой записи


    }

    public static void processFilesFromFolder(File folder) throws FileNotFoundException {
        File[] folderEntries = folder.listFiles();
        for (File entry : folderEntries) {
            if (entry.isDirectory()) {
                processFilesFromFolder(entry);
                continue;
            }

            list.add(entry);
        }
    }
}
