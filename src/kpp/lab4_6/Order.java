package kpp.lab4_6;

import java.util.Date;
import java.util.LinkedList;

public class Order {
    private int id;
    private Customer customer;
    LinkedList<Item> list;
    private int price;
    private Date date;

    public Order(Customer customer, LinkedList<Item> list, int day, int month, int year) {
        this.id = customer.getId();
        this.customer = customer;
        this.list = list;
        int sum = 0;
        for (Item item : list) {
            sum += item.getPrice();
        }
        this.price = sum;
        this.date = new Date(year, month, day);
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public LinkedList<Item> getList() {
        return list;
    }

    public int getPrice() {
        return price;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", customer=" + customer +
                ", list=" + list +
                ", price=" + price +
                ", date=" + date +
                '}'+'\n';
    }
}
