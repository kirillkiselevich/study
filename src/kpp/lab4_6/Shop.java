package kpp.lab4_6;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;


public class Shop {

    private static String[] Beginning = {"Kr", "Ca", "Ra", "Mrok", "Cru",
            "Ray", "Bre", "Zed", "Drak", "Mor", "Jag", "Mer", "Jar", "Mjol",
            "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro",
            "Mar", "Luk"};
    private static String[] Middle = {"air", "ir", "mi", "sor", "mee", "clo",
            "red", "cra", "ark", "arc", "miri", "lori", "cres", "mur", "zer",
            "marac", "zoir", "slamar", "salmar", "urak"};
    private static String[] End = {"d", "ed", "ark", "arc", "es", "er", "der",
            "tron", "med", "ure", "zur", "cred", "mur"};
    private static Logger logger;
    private static Random rand = new Random();
    static LinkedList<Item> items;
    static LinkedList<Customer> customers;
    static LinkedList<Order> orders;

    public static String generateName() {
        return Beginning[rand.nextInt(Beginning.length)] +
                Middle[rand.nextInt(Middle.length)] +
                End[rand.nextInt(End.length)];
    }

    public static void itemGenerator() {
        logger.info("item Generator");
        int num = rand.nextInt(50);
        items = new LinkedList<>();
        for (int i = 0; i < num; i++) {
            items.add(new Item(rand.nextInt(1000), rand.nextInt(1000),
                    rand.nextInt(1000), rand.nextInt(1000), rand.nextInt(1000),
                    rand.nextInt(31), rand.nextInt(12), 2017));
        }
    }

    public static void customersGenerator() {
        logger.info("customers Generator");
        int num = rand.nextInt(50);
        customers = new LinkedList<>();
        for (int i = 0; i < num; i++) {
            customers.add(new Customer(generateName(), "ul. Brestskaya d." + rand.nextInt(100) +
                    " kv." + rand.nextInt(100), "Brest", rand.nextInt(999999)));
        }
    }

    public static void orderGenerator() {
        logger.info("order Generator");
        LinkedList<Item> it = new LinkedList<>();
        orders = new LinkedList<>();
        int num = rand.nextInt(items.size());
        for (int i = 0; i < num; i++) {
            it.add(items.get(items.size() - 1));
        }
        orders.add(new Order(customers.get(rand.nextInt(customers.size())), it, rand.nextInt(31),
                rand.nextInt(12), 2017));
    }

    public static int input() {
        Scanner scanner = new Scanner(System.in);
        //return scanner.nextInt();
        return 1;
    }

    public static void main(String[] args) throws IOException {
        logger = Logger.getLogger(Shop.class.getName());
        FileHandler fileHandler = new FileHandler("app.log", true);
        logger.addHandler(fileHandler);

        itemGenerator();
        customersGenerator();
        orderGenerator();

        System.out.println(items);
        System.out.println(customers);
        System.out.println(orders);
        for (Order order : orders) {
            if (order.getId() == input()) {
                System.out.println(order.toString());
                try (FileWriter writer = new FileWriter(new File("file.txt"))) {
                    writer.write(order.toString());
                    logger.info("file saved");
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
