package kpp.lab4_6;

public class Customer {
    private int id;
    private static int idGenerator=1;
    private String Name;
    private String address;
    private String city;
    private int zipCode;

    public Customer(String Name, String address, String city, int zipCode) {
        id = idGenerator;
        idGenerator++;
        this.Name = Name;
        this.address = address;
        this.city = city;
        this.zipCode = zipCode;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", firstName='" + Name + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", zipCode=" + zipCode +
                '}'+'\n';
    }
}
