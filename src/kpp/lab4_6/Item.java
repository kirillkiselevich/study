package kpp.lab4_6;

import java.util.Date;

public class Item {
    private int price;
    private double weight;
    private int length;
    private int width;
    private int height;
    private Date date;

    public Item(int price, double weight, int length, int width, int height, int day, int month, int year) {
        this.price = price;
        this.weight = weight;
        this.length = length;
        this.width = width;
        this.height = height;
        this.date = new Date(year,month,day);
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Item{" +
                "price=" + price +
                ", weight=" + weight +
                ", length=" + length +
                ", width=" + width +
                ", height=" + height +
                ", date=" + date +
                '}'+'\n';
    }
}
