package tsos.lab2;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;

public class ContrastN1 {
    static BufferedImage inputImg, outputImg,contrastImg;

    public static void main(String[] args) throws IOException {
        inputImg = ImageIO.read(new File("./1.jpg"));
        outputImg = new BufferedImage(inputImg.getWidth(), inputImg.getHeight(), TYPE_INT_RGB);
        contrastImg = new BufferedImage(inputImg.getWidth(), inputImg.getHeight(), TYPE_INT_RGB);
        int minColor = 255, maxColor = 0;
        for (int i = 0; i < inputImg.getWidth(); i++) {
            for (int j = 0; j < inputImg.getHeight(); j++) {
                int red = new Color(inputImg.getRGB(i, j)).getRed();
                int green = new Color(inputImg.getRGB(i, j)).getGreen();
                int blue = new Color(inputImg.getRGB(i, j)).getBlue();

                int color = (int) (0.299 * red + 0.587 * green + 0.114 * blue) / 3;
//                System.out.println(color);
                if (color <= minColor)
                    minColor = color;
                if (color >= maxColor)
                    maxColor = color;
                outputImg.setRGB(i, j, (color << 16 | color << 8 | color));

            }
        }
        int gMax=255,gMin=0;
        double b =  -((gMax-gMin)/(minColor-maxColor));
        double a = gMax-b*maxColor;
        for (int i = 0; i < inputImg.getWidth(); i++) {
            for (int j = 0; j < inputImg.getHeight(); j++) {
                int color = (int)( a+  b*(new Color(outputImg.getRGB(i, j)).getRed()));
//                System.out.println(color);
                contrastImg.setRGB(i, j, (color << 16 | color << 8 | color));
            }
        }
        System.out.println("min: " + minColor + " max: " + maxColor);
        File outputfile = new File("./new-1.jpg");
        ImageIO.write(outputImg, "jpg", outputfile);
        File outputfile1 = new File("./new-2.jpg");
        ImageIO.write(contrastImg, "jpg", outputfile1);

    }
}
