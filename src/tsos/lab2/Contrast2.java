package tsos.lab2;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;

public class Contrast2 {
    static BufferedImage inputImg, outputImg, contrastImg;

    public static void main(String[] args) throws IOException {
        inputImg = ImageIO.read(new File("./1.jpg"));
        outputImg = new BufferedImage(inputImg.getWidth(), inputImg.getHeight(), TYPE_INT_RGB);
        contrastImg = new BufferedImage(inputImg.getWidth(), inputImg.getHeight(), TYPE_INT_RGB);
        int minColor = 255, maxColor = 0;
        HashMap<Integer,Integer> map = new HashMap<>();
        HashMap<Integer,Integer> colors = new HashMap<>();
        for (int i = 0; i < 256; i++) {
            map.put(i,0);
            colors.put(i,0);
        }
        int[] group = new int[256];
        int n0 = inputImg.getWidth() * inputImg.getHeight() / 256;
        for (int i = 0; i < inputImg.getWidth(); i++) {
            for (int j = 0; j < inputImg.getHeight(); j++) {
                int red = new Color(inputImg.getRGB(i, j)).getRed();
                int green = new Color(inputImg.getRGB(i, j)).getGreen();
                int blue = new Color(inputImg.getRGB(i, j)).getBlue();

                int color = (int) (0.299 * red + 0.587 * green + 0.114 * blue) / 3;
                Integer frequency = map.get(color);
                map.put(color, frequency == null ? 1 : frequency + 1);
                outputImg.setRGB(i, j, (color << 16 | color << 8 | color));
            }
        }
        System.out.println(map);
        int z=0;
        int buf=0;
        int col=0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            int n =entry.getValue()/n0;
            if(n>=1){
                colors.put(col,z);
                z++;
                col++;
                for(int i =2;i<=n;i++) {
                    colors.put(col,z++);
                    col++;
                }
                continue;
            }
            buf+=entry.getValue();
            if(buf>n0){
                colors.put(col,z);
                z++;
                buf=0;
            }
            col++;
        }
        System.out.println(n0);
        System.out.println(colors);
        for (int i = 0; i < inputImg.getWidth(); i++) {
            for (int j = 0; j < inputImg.getHeight(); j++) {
                int red = new Color(outputImg.getRGB(i, j)).getRed();
                int color= colors.get(red);
                outputImg.setRGB(i, j, (color << 16 | color << 8 | color));
            }
        }
        File outputfile = new File("./new-3.jpg");
        ImageIO.write(outputImg, "jpg", outputfile);
//        int last = 0;
//        int sum = 0;
//        int next = 0;
//        for (int i = 0; i < group.length; i++) {
//            while (group[i] < n0) {
//                if (sum < n0)
//                    sum += group[last++];
//                else {
//                    for (int j = i; j < last; j++)
//                        group[j] = next;
//                    break;
//                }
//            }
//
//            i = last;
//            next++;
//
//        }
//        for (int i : group) {
//            System.out.println(i);
//        }
    }
}
