package tsos.lab3;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;

public class Image {
    String name;
    BufferedImage inputImage, outputImage;
    int[][] gray, filter1, filter2, filter3, filter4, filter5, filter6, filter7, filter8,res;
    int limit;
    private int height;
    private int width;
    int[] filt;
    int[][] h10 = new int[][]{{1, 1, 1}, {1, -2, 1}, {-1, -1, -1}};
    int[][] h11 = new int[][]{{1, 1, 1}, {-1, -2, 1}, {-1, -1, 1}};
    int[][] h12 = new int[][]{{-1, 1, 1}, {-1, -2, 1}, {-1, 1, 1}};
    int[][] h13 = new int[][]{{-1, -1, 1}, {-1, -2, 1}, {1, 1, 1}};
    int[][] h14 = new int[][]{{-1, -1, -1}, {1, -2, 1}, {1, 1, 1}};
    int[][] h15 = new int[][]{{1, -1, -1}, {1, -2, -1}, {1, 1, 1}};
    int[][] h16 = new int[][]{{1, 1, -1}, {1, -2, -1}, {1, 1, -1}};
    int[][] h17 = new int[][]{{1, 1, 1}, {1, -2, -1}, {1, -1, -1}};

    private void init() {
        width = inputImage.getWidth();
        height = inputImage.getHeight();
        gray = new int[width][height];
        filter1 = new int[width][height];
        filter2 = new int[width][height];
        filter3 = new int[width][height];
        filter4 = new int[width][height];
        filter5 = new int[width][height];
        filter6 = new int[width][height];
        filter7 = new int[width][height];
        filter8 = new int[width][height];
        res = new int[width][height];
    }

    public Image(String name, int[] filt,int limit) throws IOException {
        this.name = name;
        this.filt = filt;
        this.limit = limit;
        inputImage = ImageIO.read(new File("./" + name + ".bmp"));
        init();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                double red = 0.299 * new Color(inputImage.getRGB(i, j)).getRed();
                double green = 0.587 * new Color(inputImage.getRGB(i, j)).getGreen();
                double blue = 0.114 * new Color(inputImage.getRGB(i, j)).getBlue();
                gray[i][j] = (int) (red + green + blue);
                filter1[i][j] = (int) (red + green + blue);
                filter2[i][j] = (int) (red + green + blue);
                filter3[i][j] = (int) (red + green + blue);
                filter4[i][j] = (int) (red + green + blue);
                filter5[i][j] = (int) (red + green + blue);
                filter6[i][j] = (int) (red + green + blue);
                filter7[i][j] = (int) (red + green + blue);
                filter8[i][j] = (int) (red + green + blue);

            }
        }
        filter();
        save();
    }

    private void filter() {
        for (int i = 1; i < width - 1; i++) {
            for (int j = 1; j < height - 1; j++) {

                int A = gray[i - 1][j - 1];
                int B = gray[i - 1][j];
                int C = gray[i - 1][j + 1];
                int D = gray[i][j - 1];
                int E = gray[i][j];
                int F = gray[i][j + 1];
                int G = gray[i + 1][j - 1];
                int H = gray[i + 1][j];
                int I = gray[i + 1][j + 1];


                filter1[i][j] = filt[0] * (h10[0][0] * A + h10[0][1] * B + h10[0][2] * C +
                        h10[1][0] * D + h10[1][1] * E + h10[1][2] * F +
                        h10[2][0] * G + h10[2][1] * H + h10[2][2] * I);
                filter2[i][j] = filt[1] * (h11[0][0] * A + h11[0][1] * B + h11[0][2] * C +
                        h11[1][0] * D + h11[1][1] * E + h11[1][2] * F +
                        h11[2][0] * G + h11[2][1] * H + h11[2][2] * I);
                filter3[i][j] = filt[2] * (h12[0][0] * A + h12[0][1] * B + h12[0][2] * C +
                        h12[1][0] * D + h12[1][1] * E + h12[1][2] * F +
                        h12[2][0] * G + h12[2][1] * H + h12[2][2] * I);
                filter4[i][j] = filt[3] * (h13[0][0] * A + h13[0][1] * B + h13[0][2] * C +
                        h13[1][0] * D + h13[1][1] * E + h13[1][2] * F +
                        h13[2][0] * G + h13[2][1] * H + h13[2][2] * I);
                filter5[i][j] = filt[4] * (h14[0][0] * A + h14[0][1] * B + h14[0][2] * C +
                        h14[1][0] * D + h14[1][1] * E + h14[1][2] * F +
                        h14[2][0] * G + h14[2][1] * H + h14[2][2] * I);
                filter6[i][j] = filt[5] * (h15[0][0] * A + h15[0][1] * B + h15[0][2] * C +
                        h15[1][0] * D + h15[1][1] * E + h15[1][2] * F +
                        h15[2][0] * G + h15[2][1] * H + h15[2][2] * I);
                filter7[i][j] = filt[6] * (h16[0][0] * A + h16[0][1] * B + h16[0][2] * C +
                        h16[1][0] * D + h16[1][1] * E + h16[1][2] * F +
                        h16[2][0] * G + h16[2][1] * H + h16[2][2] * I);
                filter8[i][j] = filt[7] * (h17[0][0] * A + h17[0][1] * B + h17[0][2] * C +
                        h17[1][0] * D + h17[1][1] * E + h17[1][2] * F +
                        h17[2][0] * G + h17[2][1] * H + h17[2][2] * I);
                int[] max = {filter1[i][j],filter2[i][j],filter3[i][j],filter4[i][j],filter5[i][j],
                        filter6[i][j],filter7[i][j],filter8[i][j]};
                Arrays.sort(max);
                res[i][j]=max[7];
            }
        }
    }

    public void save() throws IOException {
        outputImage = new BufferedImage(width, height, TYPE_INT_RGB);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int edge = res[i][j];
                edge = (edge > limit) ? 255 : 0;
                outputImage.setRGB(i, j, (edge << 16 | edge << 8 | edge));
            }
        }
        File outputfile = new File("./res-" + name + ".bmp");
        ImageIO.write(outputImage, "bmp", outputfile);
    }
}
