package tsos.lab1;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;

public class Interpol {
    static BufferedImage inputImg, outputImg;

    public static void main(String[] args) throws IOException {
        double scale = 1.5;
        inputImg = ImageIO.read(new File("./1.jpg"));
        outputImg = new BufferedImage((int) (inputImg.getWidth() * scale), (int) (inputImg.getHeight() * scale), TYPE_INT_RGB);
        zoomImage(scale);
        JFrame frame = new JFrame();
        frame.getContentPane().setLayout(new FlowLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new JLabel(new ImageIcon(inputImg)));
        frame.getContentPane().add(new JLabel(new ImageIcon(outputImg)));
        frame.pack();
        frame.setVisible(true);


    }

    public static void zoomImage(double scaleFactor) throws IOException {
        for (int i = 0; i < outputImg.getWidth() - 1; i++) {
            for (int j = 0; j < outputImg.getHeight() - 1; j++) {
                double srcX = i / scaleFactor;
                double srcY = j / scaleFactor;
                int srcX1 = (int) srcX - 1;
                int srcX2 = (int) srcX + 1;
                int srcY1 = (int) srcY - 1;
                int srcY2 = (int) srcY + 1;
                if (srcX1 < 0) {
                    srcX1++;
                    srcX += scaleFactor;
                }
                if (srcY1 < 0) {
                    srcY1++;
                    srcY += scaleFactor;
                }
                if (srcX2 >= outputImg.getWidth()) {
                    srcX2--;
                    srcX -= scaleFactor;
                }
                if (srcY2 >= outputImg.getHeight()) {
                    srcY2--;
                    srcY -= scaleFactor;
                }

                double[] coeff = new double[4];// весовые коэфы для каждого из соседей
                coeff[0] = 1 / (srcX - srcX1);         // для левого соседа
                coeff[2] = 1 / (srcX2 - srcX);         // для правого соседа
                coeff[1] = 1 / (srcY2 - srcY);         // для нижнего соседа
                coeff[3] = 1 / (srcY - srcY1);         // для верхнего соседа
                double coeffSum = 0;
                for (int k = 0; k < 4; k++)
                    coeffSum += coeff[k];
                int[] red = new int[4];
                int[] green = new int[4];
                int[] blue = new int[4];

                red[0] = (int) (new Color(inputImg.getRGB(srcX1, (int) srcY)).getRed() * coeff[0] / coeffSum);
                red[2] = (int) (new Color(inputImg.getRGB(srcX2, (int) srcY)).getRed() * coeff[2] / coeffSum);
                red[1] = (int) (new Color(inputImg.getRGB((int) srcX, srcY2)).getRed() * coeff[1] / coeffSum);
                red[3] = (int) (new Color(inputImg.getRGB((int) srcX, srcY1)).getRed() * coeff[3] / coeffSum);

                green[0] = (int) (new Color(inputImg.getRGB(srcX1, (int) srcY)).getGreen() * coeff[0] / coeffSum);
                green[2] = (int) (new Color(inputImg.getRGB(srcX2, (int) srcY)).getGreen() * coeff[2] / coeffSum);
                green[1] = (int) (new Color(inputImg.getRGB((int) srcX, srcY2)).getGreen() * coeff[1] / coeffSum);
                green[3] = (int) (new Color(inputImg.getRGB((int) srcX, srcY1)).getGreen() * coeff[3] / coeffSum);

                blue[0] = (int) (new Color(inputImg.getRGB(srcX1, (int) srcY)).getBlue() * coeff[0] / coeffSum);
                blue[2] = (int) (new Color(inputImg.getRGB(srcX2, (int) srcY)).getBlue() * coeff[2] / coeffSum);
                blue[1] = (int) (new Color(inputImg.getRGB((int) srcX, srcY2)).getBlue() * coeff[1] / coeffSum);
                blue[3] = (int) (new Color(inputImg.getRGB((int) srcX, srcY1)).getBlue() * coeff[3] / coeffSum);

                int sumRed = 0, sumGreen = 0, sumBlue = 0;
                for (int k = 0; k < blue.length; k++) {
                    sumRed += red[k];
                    sumGreen += green[k];
                    sumBlue += blue[k];

                }
                outputImg.setRGB(i, j, (sumRed << 16 | sumGreen << 8 | sumBlue));
            }
        }

        File outputfile = new File("./new-1.jpg");
        ImageIO.write(outputImg, "jpg", outputfile);


    }
}
