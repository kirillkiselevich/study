package tsos.lab4;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;

public class Main {
    static BufferedImage inputImg, outputImg, outputImg1;
    static int n;
    public static void main(String[] args) throws IOException {
        inputImg = ImageIO.read(new File("./1.jpg"));
        outputImg = new BufferedImage(inputImg.getWidth(), inputImg.getHeight(), TYPE_INT_RGB);
        outputImg1 = new BufferedImage(inputImg.getWidth(), inputImg.getHeight(), TYPE_INT_RGB);
        n=1;
        pack();
        saveOut();
        unpack();
    }

    public static void save() throws IOException {
        outputImg = new BufferedImage(inputImg.getWidth(), inputImg.getHeight(), TYPE_INT_RGB);
        for (int i = 0; i < inputImg.getWidth(); i++) {
            for (int j = 0; j < inputImg.getHeight(); j++) {
                double red = new Color(inputImg.getRGB(i, j)).getRed();
                double green = new Color(inputImg.getRGB(i, j)).getGreen();
                double blue = new Color(inputImg.getRGB(i, j)).getBlue();
                outputImg.setRGB(i, j, ((int) red << 16 | (int) green << 8 | (int) blue));
            }
        }
        File outputfile = new File("./l4-1.jpg");
        ImageIO.write(outputImg, "jpg", outputfile);
    }
    public static void saveOut() throws IOException {
        outputImg1 = new BufferedImage(outputImg.getWidth(), inputImg.getHeight(), TYPE_INT_RGB);
        for (int i = 0; i < inputImg.getWidth(); i++) {
            for (int j = 0; j < inputImg.getHeight(); j++) {
                double red = new Color(outputImg.getRGB(i, j)).getRed();
                double green = new Color(outputImg.getRGB(i, j)).getGreen();
                double blue = new Color(outputImg.getRGB(i, j)).getBlue();
                outputImg1.setRGB(i, j, ((int) red << 16 | (int) green << 8 | (int) blue));
            }
        }
        File outputfile = new File("./l4-1.jpg");
        ImageIO.write(outputImg1, "jpg", outputfile);
    }

    public static void pack() throws IOException {
        int height = inputImg.getWidth(), width = inputImg.getHeight();
        int[][] b1R, b1B, b1G, b2R, b2B, b2G, b3R, b3B, b3G, b4R, b4B, b4G;
        save();
        for (int k = 0; k < n; k++) {
            height /= 2;
            width /= 2;
            b1R = new int[width][height];
            b1G = new int[width][height];
            b1B = new int[width][height];
            b2B = new int[width][height];
            b2R = new int[width][height];
            b2G = new int[width][height];
            b3R = new int[width][height];
            b3B = new int[width][height];
            b3G = new int[width][height];
            b4G = new int[width][height];
            b4B = new int[width][height];
            b4R = new int[width][height];
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    b1R[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getRed()
                            + new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getRed() +
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getRed() +
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getRed()) / 4.0);
                    b1G[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getGreen()
                            + new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getGreen() +
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getGreen() +
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getGreen()) / 4.0);
                    b1B[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getBlue()
                            + new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getBlue() +
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getBlue() +
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getBlue()) / 4.0);

                    b2R[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getRed()
                            + new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getRed() -
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getRed() -
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getRed()) / 4.0);
                    b2G[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getGreen()
                            + new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getGreen() -
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getGreen() -
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getGreen()) / 4.0);
                    b2B[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getBlue()
                            + new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getBlue() -
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getBlue() -
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getBlue()) / 4.0);

                    b3R[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getRed()
                            - new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getRed() +
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getRed() -
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getRed()) / 4.0);
                    b3G[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getGreen()
                            - new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getGreen() +
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getGreen() -
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getGreen()) / 4.0);
                    b3B[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getBlue()
                            - new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getBlue() +
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getBlue() -
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getBlue()) / 4.0);

                    b4R[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getRed()
                            - new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getRed() -
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getRed() +
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getRed()) / 4.0);
                    b4G[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getGreen()
                            - new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getGreen() -
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getGreen() +
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getGreen()) / 4.0);
                    b4B[i][j] = (int) ((double) (new Color(outputImg.getRGB(2 * i, 2 * j)).getBlue()
                            - new Color(outputImg.getRGB(2 * i + 1, 2 * j)).getBlue() -
                            new Color(outputImg.getRGB(2 * i, 2 * j + 1)).getBlue() +
                            new Color(outputImg.getRGB(2 * i + 1, 2 * j + 1)).getBlue()) / 4.0);
                }
            }
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    outputImg.setRGB(i, j, (b1R[i][j] << 16 | b1G[i][j] << 8 | b1B[i][j]));
                    outputImg.setRGB(i + width, j, ((Math.abs(b2R[i][j])) << 16 | (Math.abs(b2G[i][j])) << 8 | (Math.abs(b2B[i][j]))));
                    outputImg.setRGB(i, j + height, ((Math.abs(b3R[i][j])) << 16 | (Math.abs(b3G[i][j])) << 8 | (Math.abs(b3B[i][j]))));
                    outputImg.setRGB(i + width, j + height, ((Math.abs(b4R[i][j])) << 16 | (Math.abs(b4G[i][j])) << 8 | (Math.abs(b4B[i][j]))));
                }
            }
        }
        File outputfile = new File("./l4-1.jpg");
        ImageIO.write(outputImg, "jpg", outputfile);
    }

    public static void unpack() throws IOException {
        int height = outputImg.getWidth(), width = outputImg.getHeight();

        int[][] b1R, b1B, b1G, b2R, b2B, b2G, b3R, b3B, b3G, b4R, b4B, b4G;
        for (int k = n; k >= 1; k--) {
            height /= Math.pow(2, k);
            width /= Math.pow(2, k);
            b1R = new int[width][height];
            b1G = new int[width][height];
            b1B = new int[width][height];
            b2B = new int[width][height];
            b2R = new int[width][height];
            b2G = new int[width][height];
            b3R = new int[width][height];
            b3B = new int[width][height];
            b3G = new int[width][height];
            b4G = new int[width][height];
            b4B = new int[width][height];
            b4R = new int[width][height];
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    b1R[i][j] = new Color(outputImg1.getRGB(i, j)).getRed();
                    b1G[i][j] = new Color(outputImg1.getRGB(i, j)).getGreen();
                    b1B[i][j] = new Color(outputImg1.getRGB(i, j)).getBlue();
                    b2R[i][j] = new Color(outputImg1.getRGB(i+width, j)).getRed();
                    b2G[i][j] = new Color(outputImg1.getRGB(i+width, j)).getGreen();
                    b2B[i][j] = new Color(outputImg1.getRGB(i+width, j)).getBlue();
                    b3R[i][j] = new Color(outputImg1.getRGB(i, j+height)).getRed();
                    b3G[i][j] = new Color(outputImg1.getRGB(i, j+height)).getGreen();
                    b3B[i][j] = new Color(outputImg1.getRGB(i, j+height)).getBlue();
                    b4R[i][j] = new Color(outputImg1.getRGB(i+width, j+height)).getRed();
                    b4G[i][j] = new Color(outputImg1.getRGB(i+width, j+height)).getGreen();
                    b4B[i][j] = new Color(outputImg1.getRGB(i+width, j+height)).getBlue();
                }
            }
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    int a1R =(b1R[i][j]+b2R[i][j]+b3R[i][j]+b4R[i][j]);
                    int a1G = (b1G[i][j]+b2G[i][j]+b3G[i][j]+b4G[i][j]);
                    int a1B = (b1B[i][j]+b2B[i][j]+b3B[i][j]+b3B[i][j]);
                    int a2R = 2*b1R[i][j] +  2*b2R[i][j] - a1R;
                    int a2G = 2*b1G[i][j] +  2*b2G[i][j] - a1G;
                    int a2B = 2*b1B[i][j] +  2*b2B[i][j] - a1B;
                    int a3R = 2*b1R[i][j] +  2*b3R[i][j] - a1R;
                    int a3G = 2*b1G[i][j] +  2*b3G[i][j] - a1G;
                    int a3B = 2*b1B[i][j] +  2*b3B[i][j] - a1B;
                    int a4R = 2*b1R[i][j] +  2*b4R[i][j] - a1R;
                    int a4G = 2*b1G[i][j] +  2*b4G[i][j] - a1G;
                    int a4B = 2*b1B[i][j] +  2*b4B[i][j] - a1B;
                    outputImg1.setRGB(2*i, 2*j, (a1R << 16 | a1G<< 8 | a1B));
                    outputImg1.setRGB(2*i + 1, 2*j, (a2R << 16 | a2G<< 8 | a2B));
                    outputImg1.setRGB(2*i, 2*j + 1, (a3R << 16 | a3G<< 8 | a3B));
                    outputImg1.setRGB(2*i + 1, 2*j + 1, (a4R << 16 | a4G<< 8 | a4B));
                }
            }
            File outputfile = new File("./l4-2.jpg");
            ImageIO.write(outputImg1, "jpg", outputfile);
            height = outputImg.getWidth();
            width = outputImg.getHeight();
        }


    }
}