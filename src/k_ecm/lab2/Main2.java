package k_ecm.lab2;

import java.math.BigInteger;
import java.util.ArrayList;

public class Main2 {
    public static void main(String[] args) {
        ArrayList<Double> list = Reserv(30,15,0.9);
        for (int i = 0; i < list.size(); i++) {
            System.out.printf("P [%d] = %f\n",i+1,Math.pow(list.get(i),1));
        }
    }

    public static BigInteger factorial(int n) {
        BigInteger f = BigInteger.valueOf(1);
        for (int i = 2; i <= n; i++) f=f.multiply(BigInteger.valueOf(i));
        return f;
    }
    public static double C(int n, int k) {
        return Double.parseDouble(factorial(n).divide(factorial(k)).divide(factorial(n-k)).toString());
    }

    public static ArrayList<Double> Reserv(int n, int rMax, double P1) {
        double P=0.0;
        ArrayList<Double> list = new ArrayList<>();
        for (int r = 1; r<=rMax; r++) {
            P=0.0;
            for (int i = 0; i<=r; i++) {
                P+=(C((n+r),i)*Math.pow(P1,n+r-i)*Math.pow((1-P1),i));
            }
            list.add(P);
        }
        return list;
    }
}
