package k_ecm.lab2;

public class Main5 {
    public static void main(String[] args) {
        System.out.printf("P [%d] = %f\n",0,Reserv(4,0,0.91));
        System.out.printf("P [%d] = %f\n",2,Reserv(4,2,0.91));
    }
    public static double Reserv(int N, int R, double P1) {
        double P=0.0;
        for (int i = 0; i<=R; i++) {
            P+=(Main2.C((N+R),i)*Math.pow(P1,N+R-i)*Math.pow((1-P1),i));
        }
        return P;
    }
}
