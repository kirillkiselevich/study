package k_ecm.lab2;

import java.math.BigInteger;
import java.util.Scanner;

public class Main4 {
    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);
        double psd,p1;
        System.out.println("PSD:");
        psd = Double.parseDouble(src.next());
        System.out.println("P1:");
        p1 = Double.parseDouble(src.next());
        System.out.println(Reserv(psd,p1));
    }

    public static BigInteger factorial(int n) {
        BigInteger f = BigInteger.valueOf(1);
        for (int i = 2; i <= n; i++) f=f.multiply(BigInteger.valueOf(i));
        return f;
    }
    public static double C(int n, int k) {
        return Double.parseDouble(factorial(n).divide(factorial(k)).divide(factorial(n-k)).toString());
    }

    public static double Reserv(double Psd, double P1) {
        double P=0.0;
        for (int i = 0; i<=1; i++) {
            P+=(C((2),i)*Math.pow(P1,2-i)*Math.pow((1-P1),i));
        }
        return Math.pow(Psd,2)*P;
    }
}
